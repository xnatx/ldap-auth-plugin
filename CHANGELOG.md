# Changelog

## 1.2.1
[Released](https://bitbucket.org/xnatx/ldap-auth-plugin/src/1.2.1/)
* **Improvement** XNAT User Ids match the case of the Active Directory userid. For backward compatibility, existing LDAP+XNAT accounts continue to work as is.
